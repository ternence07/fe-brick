### HTML全局属性有哪些？（包含h5）

全局属性 可用于任何 HTML 元素。

| 属性 | 描述 |
| :-----: | :---- |
| accesskey	| 设置访问元素的键盘快捷键。| 
| class	| 规定元素的类名（classname）| 
| contenteditableNew | 	规定是否可编辑元素的内容。| 
| contextmenuNew | 	指定一个元素的上下文菜单。当用户右击该元素，出现上下文菜单| 
| data-*New	| 用于存储页面的自定义数据| 
| dir| 	设置元素中内容的文本方向。 | 
| draggableNew	| 指定某个元素是否可以拖动 | 
| dropzoneNew	| 指定是否将数据复制，移动，或链接，或删除 | 
| hiddenNew	hidden |  属性规定对元素进行隐藏。 | 
| id	| 规定元素的唯一 id | 
| lang | 	设置元素中内容的语言代码。 | 
| spellcheckNew | 	检测元素是否拼写错误 | 
| style	| 规定元素的行内样式（inline style）| 
| tabindex	| 设置元素的 Tab 键控制次序。| 
| title	| 规定元素的额外信息（可在工具提示中显示）| 
| translateNew	| 指定是否一个元素的值在页面载入时是否需要翻译 | 


### 在页面上隐藏元素的方法有哪些？

占位:

- visibility: hidden;
- margin-left: -100%;
- opacity: 0;
- transform: scale(0);

不占位:

- display: none;
- width: 0; height: 0; overflow: hidden;

仅对块内文本元素:

- text-indent: -9999px;
- font-size: 0;


### 去除字符串中最后一个字符

```js
/**
 * 去除字符串中最后一个字符
 * 拓展：删除字符串中的某个位置的某个字符
 */

const  delLastChar = (str,char) =>{
    return str.split('').reverse().join('').replace(char,'').split('').reverse().join('')
}

console.log((delLastChar('abacadaeafagah','a')))

const group=(array, subGroupLength)=> {
    return [array.slice(0,subGroupLength),array.slice(subGroupLength)];
}

const delChar= (str,char,index) => {
    const result=[]
    const list = group(str.split(char),index)
    for (const listElement of list) {
        result.push(listElement.join(char))
    }
    return result.join('')
}

console.log((delChar('abacadaeafagah','a',10)))
```
