### 简述超链接target属性的取值和作用

`a`标签`target`属性一共有4个值：

- `_self`

  默认属性。在当前窗口或者框架中加载目标文档

- `_blank`

  打开新的窗口或者新的标签页。在使用这个属性时，最好添加`rel="noopener norefferrer"`属性，防止打开的新窗口对原窗口进行篡改。防止 window.opener API 的恶意行为。

- `_parent`

  在`frame`或者`iframe`中使用较多。在父级框架中载入目标文档，当`a`标签本身在顶层时，与`_self`相同。

- `_top`

  在`frame`或者`iframe`中使用较多。直接在顶层的框架中载入目标文档，加载整个窗口

### css新增伪类有哪些，并简述

| 属性 | 描述 |
| :-----: | :---- |
| :root	| 文档根元素，总是返回html。| 
| :last-child, :only-child, :only-of-type	| 文本的最后一个 / 唯一一个 / 指定类型的唯一一个 子元素 | 
| :nth-child(n), :nth-last-child(n), :nth-of-type(n), :nth-last-of-type(n),	| 第n个 / 倒数第n个 / 指定类型的第n个 / 指定类型的倒数第n个 子元素 | 
| :enabled, :disabled	| 	启用 / 禁用 | 
| :checked	| 已勾选 | 
| :default	| 默认，例如radio group中默认选中的radio | 
| :valid, :invalid, :required, :optional, :in-range, :out-of-range	| 校验有效 / 校验无效 / 必填 / 非必填 / 限定范围内 / 限定范围外的 input | 
| :not()	| 括号内条件取反 | 
| :empty	| 没有子元素的元素 | 
| :target	| URL片段标识符指向的元素 | 
	
### 写一个把字符串大小写切换的方法

```js
const caseSwitch=(str)=>{
    return str.split('').map(char=>{
        if((/[a-z]/).test(char)){
            return char.toUpperCase()
        }
        if((/[A-Z]/).test(char)){
            return char.toLowerCase()
        }
        return char
    }).join('')
}

console.log('原字符串 =====》','adaASdaaAdadaagaga')
console.log('转换后 =====》',caseSwitch('adaASdaaAdadaagaga'))
```
	
	
