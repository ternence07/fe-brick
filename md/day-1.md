### 页面导入样式时，使用link和@import有什么区别？

- 区别1：link是XHTML标签，除了加载CSS外，还可以定义RSS等其他事务；@import属于CSS范畴，只能加载CSS。
- 区别2：link引用CSS时，在页面载入时同时加载；@import需要页面网页完全载入以后加载。 所以会出现一开始没有css样式，闪烁一下出现样式后的页面(网速慢的情况下)
- 区别3：link是XHTML标签，无兼容问题；@import是在CSS2.1提出的，低版本的浏览器不支持。
- 区别4：link支持使用Javascript控制DOM去改变样式；而@import不支持

在html设计制作中，css有四种引入方式。

#### 内联样式

内联样式，也叫行内样式，指的是直接在 HTML 标签中的 style 属性中添加 CSS。

示例：

```html
 <div style="display: none;background:red"></div>
```

这通常是个很糟糕的书写方式，它只能改变当前标签的样式，如果想要多个拥有相同的样式，你不得不重复地为每个添加相同的样式，如果想要修改一种样式，又不得不修改所有的 style 中的代码。很显然，内联方式引入 CSS 代码会导致 HTML 代码变得冗长，且使得网页难以维护。

#### 嵌入样式

嵌入方式指的是在 HTML 头部中的 `<style>` 标签下书写 CSS 代码。

示例：

```html
<head>
    <style>
        .content {
            background: red;
        }
    </style>
    <title></title>
</head>
```

嵌入方式的 CSS 只对当前的网页有效。因为 CSS 代码是在 HTML 文件中，所以会使得代码比较集中，当我们写模板网页时这通常比较有利。因为查看模板代码的人可以一目了然地查看 HTML 结构和 CSS 样式。因为嵌入的 CSS 只对当前页面有效，所以当多个页面需要引入相同的 CSS 代码时，这样写会导致代码冗余，也不利于维护

#### 链接样式

链接方式指的是使用 HTML 头部的 标签引入外部的 CSS 文件。

示例：

```html

<head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <title></title>
</head>
```

这是最常见的也是最推荐的引入 CSS 的方式。使用这种方式，所有的 CSS 代码只存在于单独的 CSS 文件中，所以具有良好地可维护性。并且所有的 CSS 代码只存在于 CSS 文件中，CSS 文件会在第一次加载时引入，以后切换页面时只需加载 HTML 文件即可

#### 导入样式

导入方式指的是使用 CSS 规则引入外部 CSS 文件。

示例：

```html
<style>
    @import url(style.css);
</style>
```

或者写在css样式中

```css
@charset "utf-8";
@import url(style.css);
*{ margin:0; padding:0;}
.notice-link a{ color:#999;}
```

### 圣杯布局和双飞翼布局的理解和区别，并用代码实现

作用：圣杯布局和双飞翼布局解决的问题是一样的，就是两边顶宽，中间自适应的三栏布局，中间栏要在放在文档流前面以优先渲染。

区别：圣杯布局，为了中间div内容不被遮挡，将中间div设置了左右padding-left和padding-right后，将左右两个div用相对布局position: relative并分别配合right和left属性，以便左右两栏div移动后不遮挡中间div。双飞翼布局，为了中间div内容不被遮挡，直接在中间div内部创建子div用于放置内容，在该子div里用margin-left和margin-right为左右两栏div留出位置

圣杯布局：

```html
<html lang="">
<body>
<div id="hd">header</div>
<div id="bd">
  <div id="middle">middle</div>
  <div id="left">left</div>
  <div id="right">right</div>
</div>
<div id="footer">footer</div>
</body>

<style>
#hd{
    height:50px;
    background: #666;
    text-align: center;
}
#bd{
    /*左右栏通过添加负的margin放到正确的位置了，此段代码是为了摆正中间栏的位置*/
    padding:0 200px 0 180px;
    height:100px;
}
#middle{
    float:left;
    width:100%;/*左栏上去到第一行*/
    height:100px;
    background:blue;
}
#left{
    float:left;
    width:180px;
    height:100px;
    margin-left:-100%;
    background:#0c9;
    /*中间栏的位置摆正之后，左栏的位置也相应右移，通过相对定位的left恢复到正确位置*/
    position:relative;
    left:-180px;
}
#right{
    float:left;
    width:200px;
    height:100px;
    margin-left:-200px;
    background:#0c9;
    /*中间栏的位置摆正之后，右栏的位置也相应左移，通过相对定位的right恢复到正确位置*/
    position:relative;
    right:-200px;
}
#footer{
    height:50px;
    background: #666;
    text-align: center;
}
</style>
</html>
```

双飞翼布局:

```html
<body>
<div id="hd">header</div>
<div id="bd">
    <div id="middle">middle</div>
    <div id="left">left</div>
    <div id="right">right</div>
</div>
<div id="footer">footer</div>
</body>

<style>
    #hd{
        height:50px;
        background: #666;
        text-align: center;
    }
    #bd{
        /*左右栏通过添加负的margin放到正确的位置了，此段代码是为了摆正中间栏的位置*/
        padding:0 200px 0 180px;
        height:100px;
    }
    #middle{
        float:left;
        width:100%;/*左栏上去到第一行*/
        height:100px;
        background:blue;
    }
    #left{
        float:left;
        width:180px;
        height:100px;
        margin-left:-100%;
        background:#0c9;
        /*中间栏的位置摆正之后，左栏的位置也相应右移，通过相对定位的left恢复到正确位置*/
        position:relative;
        left:-180px;
    }
    #right{
        float:left;
        width:200px;
        height:100px;
        margin-left:-200px;
        background:#0c9;
        /*中间栏的位置摆正之后，右栏的位置也相应左移，通过相对定位的right恢复到正确位置*/
        position:relative;
        right:-200px;
    }
    #footer{
        height:50px;
        background: #666;
        text-align: center;
    }
</style>
```

### 用递归算法实现，数组长度为5且元素的随机数在2-32间不重复的值

#### 解析

- 生成一个长度为5的空数组arr
- 生成一个2～32的随机数
- 随机数插入数组判断是否已存在，存在重新生成，并插入
- 最终输出一个长度5，且不重复的数组

```js
const createArr =(min,max,len)=>{

    const randomNumber =(min,max)=>{
        return Math.floor(Math.random()*(max - 1) + 2)
    }

    const arr = new Array(len)
    const num = randomNumber(min,max)
    let i = 0

    const randomArr =(arr,num)=>{
        if (arr.indexOf(num)< 0){
            arr[i] = num;
            i++;
        } else {
            num = randomNumber(min,max);
        }
        if (i>=arr.length){
            return arr;
        }else{
            return randomArr(arr,num)
        }
    }
    return  randomArr(arr,num)
}

console.log(createArr(2,32,5))
```
