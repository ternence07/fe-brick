### html的元素有哪些（包含H5）？

#### 要求区分行内元素，块元素，空元素，并说明其作用

块级元素:

- 总是在新行上开始；
- 高度，行高以及外边距和内边距都可控制；
- 宽度缺省是它的容器的100%，除非设定一个宽度。
- 它可以容纳内联元素和其他块元素

| 元素 | 说明 |
| :-----: | :---- |
| address | 地址 |
| blockquote | 块引用 |
| center | 居中对齐块 |
| dir | 目录列表 |
| div | 常用块级容器，也是css layout的主要标签 |
| dl | 定义列表 |
| fieldset | form控制组 |
| form | 交互表单 |
| h1 | 大标题 |
| h2 | 副标题 |
| h3 | 3级标题 |
| h4 | 4级标题 |
| h5 | 5级标题 |
| h6 | 6级标题 |
| hr | 水平分隔线 |
| menu |  菜单列表 |
| noframes |  frames可选内容，（对于不支持frame的浏览器显示此区块内容） |
| noscript |  可选脚本内容（对于不支持script的浏览器显示此内容） |
| ol |  排序表单 |
| p |  段落 |
| pre |  格式化文本 |
| table |   表格 |
| ul |   非排序列表（无序列表） |



内联元素：

- 和其他元素都在一行上；
- 高，行高及外边距和内边距不可改变；
- 宽度就是它的文字或图片的宽度，不可改变
- 内联元素只能容纳文本或者其他内联元素

| 元素 | 说明 |
| :-----: | :---- |
| a | 锚点 |
| abbr | 缩写 |
| aronym | 首字 |
| b | 粗体(不推荐) |
| bdo | bidi override |
| big | 大字体 |
| br | 换行 |
| cite | 引用 |
| code | 计算机代码(在引用源码的时候需要) |
| dfn | 定义字段 |
| em | 强调 |
| font | 字体设定(不推荐) |
| i | 斜体 |
| img | 图片 |
| input | 输入框 |
| kbd | 定义键盘文本 |
| label | 表格标签 |
| q | 短引用 |
| s | 中划线(不推荐) |
| samp | 定义范例计算机代码 |
| select |  项目选择 |
| small | 小字体文本 |
| span | 常用内联容器，定义文本内区块 |
| strike | 中划线 |
| strong | 粗体强调 |
| sub | 下标 |
| sup | 上标 |
| textarea | 多行文本输入框 |
| tt | 电传文本 |
| u |  下划线 |
| var | 定义变量 |


可变元素：

可变元素由上下文语境来决定是块元素还是内联元素。

| 元素 | 说明 |
| :-----: | :---- |
| applet |  java applet |
| button |  按钮 |
| del | 删除文本 |
| iframe | inline frame |
| ins | 插入的文本 |
| map | 图片区块（map） |
| object | object对象 |
| script | 客户端脚本 |

空元素：

HTML，SVG，或者 MathML 里的一个不能存在子节点（例如内嵌的元素或者元素内的文本）的element。
HTML，SVG 和 MathML 的规范都详细定义了每个元素能包含的具体内容（define very precisely what each element can contain）。许多组合是没有任何语义含义的，比如一个元素嵌套在一个元素里。
在 HTML 中，通常在一个空元素上使用一个闭标签是无效的。

| 元素 | 说明 |
| :-----: | :---- |
| area |  热点区域，可以关联一个超链接 |
| base |  一个文档中包含的所有相对 URL 的根 UR |
| br |  生成一个换行（回车）符号 |
| col |  定义表格中的列，并用于定义所有公共单元格上的公共语义 |
| colgroup |  定义表中的一组列表 |
| command |   |
| embed |  外部内容嵌入文档中的指定位置 |
| hr |  段落级元素之间的主题转换（例如，一个故事中的场景的改变，或一个章节的主题的改变） |
| img |  图像嵌入文档 |
| input |  输入框 |
| link |  当前文档与外部资源的关系 |
| meta |   |
| param |  定义参数 |
| source |  媒体资源 |
| track |   |
| wbr |  文本中的位置，其中浏览器可以选择来换行，虽然它的换行规则可能不会在这里换行 |


### CSS3有哪些新增的特性

#### 边框(borders):

border-radius 圆角

box-shadow 盒阴影

border-image 边框图像

#### 背景:

background-size 背景图片的尺寸

background_origin 背景图片的定位区域

background-clip 背景图片的绘制区域

#### 渐变：

linear-gradient 线性渐变

radial-gradient 径向渐变

#### 文本效果

word-break

word-wrap

text-overflow

text-shadow

text-wrap

text-outline

text-justify


#### 2D转换：

##### 2D转换属性

transform

transform-origin

##### 2D转换方法

translate(x,y)

translateX(n)

translateY(n)

rotate(angle)

scale(n)

scaleX(n)

scaleY(n)

rotate(angle)

matrix(n,n,n,n,n,n)

#### 3D转换：

##### 3D转换属性：

transform

transform-origin

transform-style

##### 3D转换方法

translate3d(x,y,z)

translateX(x)

translateY(y)

translateZ(z)

scale3d(x,y,z)

scaleX(x)

scaleY(y)

scaleZ(z)

rotate3d(x,y,z,angle)

rotateX(x)

rotateY(y)

rotateZ(z)

perspective(n)

#### 过渡

transition

#### 动画

@Keyframes规则

animation

####弹性盒子(flexbox)

####多媒体查询@media


### 写一个方法去掉字符串中的空格

```js

const str = '  s t  r  '

const POSITION = Object.freeze({
    left: Symbol(),
    right: Symbol(),
    both: Symbol(),
    center: Symbol(),
    all: Symbol(),
})

const  trim = (str,position=POSITION.both) => {
    if (!!POSITION[position]) throw new Error('unexpected position value')

    switch(position) {
        case(POSITION.left):
            str = str.replace(/^\s+/, '')
            break;
        case(POSITION.right):
            str = str.replace(/\s+$/, '')
            break;
        case(POSITION.both):
            str = str.replace(/^\s+/, '').replace(/\s+$/, '')
            break;
        case(POSITION.center):
            while (str.match(/\w\s+\w/)) {
                str = str.replace(/(\w)(\s+)(\w)/, `$1$3`)
            }
            break;
        case(POSITION.all):
            str = str.replace(/\s/g, '')
            break;
        default:
    }

    return str

}

const result = trim(str,POSITION.both)

console.log(`|${result}|`)
```
