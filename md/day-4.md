### HTML5的文件离线存储怎么使用，工作原理是什么？

#### 优点：

没有网络时可以浏览,加快资源的加载速度,减少服务器负载

#### 使用

只需要在页面头部加入,然后创建manifest.appcache文件

#### manifest.appcache文件配置:

1. CACHE MANIFEST放在第一行
2. CACHE:表示需要离线存储的资源列表,由于包含manifest文件的页面将被自动离线存储,所以不需要列出来
3. NETWORK:表示在线才能访问的资源列表,如果CACHE列表里也存在,则CACHE优先级更高
4. FALLBACK:表示如果访问第一个资源是吧,那么使用第二个资源来替换它

#### 浏览器如何解析manifest

1. 在线情况:浏览器发现html头部有manifest属性,他会请求manifest文件,如果是第一次访问,那么浏览器会根据manifest文件的内容下载相应的资源并且进行离线存储.如果已经访问过并存储,那么浏览器使用 离线的资源价值,然后对比新的文件,如果没有发生改变就不做任何操作,如果文件改变了,那么就会重新下载文件中的资源并进行离线存储
2. 离线情况:浏览器就直接使用离线存储资源

#### 与传统浏览器的区别

1. 离线缓存是针对整个应用,浏览器缓存是单个文件
2. 离线缓存可以主动通知浏览器更新资源

#### 状态 window.applicationCache对象的status属性

- 0: 无缓存
- 1:闲置
- 2.检查中,正在下载描述文件并检查更新
- 3:下载中
- 4:更新完成
- 5:废弃,应用缓存的描述文件已经不存在了,因此页面无法再访问应用缓存

#### 事件 window.applicationCache对象的相关事件

- oncached:当离线资源存储完成之后就触发这个事件
- onchecking:当浏览器对离线存储资源进行更新检查的时候触发
- ondounloading:当浏览器开始下载离线资源的时候会触发
- onprogress:当浏览器在下载每一个资源的时候会触发
- onupdateready:当浏览器对离线资源更新完成之后触发
- onnoupdate:当浏览器检查更新之后发现没有这个资源时触发

#### 注意事项

- 站点离线存储的容量限制是5M
- 如果manifest文件,或者内部列举的某一个文件不能正常下载,整个更新过程将视为失败,浏览器继续全部使用老的缓存
- 引用manifest的html必须与manifest文件同源,在同一个域下
- 在manifest中使用的相对路径,相对参照物为manifest文件
- CACHE MANIFEST字符串硬在第一行,且必不可少
- 系统会自动缓存引用清单文件的HTML文件
- manifest文件中CACHE则与NETWORK，FALLBACK的位置顺序没有关系，如果是隐式声明需要在最前面
- FALLBACK中的资源必须和manifest文件同源
- 当一个资源被缓存后，该浏览器直接请求这个绝对路径也会访问缓存中的资源。
- 站点中的其他页面即使没有设置manifest属性，请求的资源如果在缓存中也从缓存中访问
- 当manifest文件发生改变时，资源请求本身也会触发更新


### CSS选择器器有哪些，哪些属性可以继承？

常见的CSS选择器：

- id选择器 

  设置标签的 id 属性来设置id选择器。CSS 中 id 选择器以 # 来定义

- 类选择器

  类选择器通过设置标签的 class 属性去设置样式。CSS 中 class 选择器以 . 来定义。

- 标签选择器

  标签选择器是为某一类标签设置 CSS 样式。在 CSS 中直接以标签名设置样式

- 内联选择器

  在标签内写 CSS 代码。通过加上 style 属性，就可以在 style 内添加 CSS 样式了


可继承的属性：

- 字体系列属性

  | 属性 | 说明 |
  | :-----: | :---- |
  | font | 组合字体 | 
  | font-family | 规定元素的字体系列 |
  | font-weight | 设置字体的粗细 |
  | font-size | 设置字体的尺寸 |
  | font-style | 定义字体的风格 |
  | font-variant | 设置小型大写字母的字体显示文本，这意味着所有的小写字母均会被转换为大写，但是所有使用小型大写字体的字母与其余文本相比，其字体尺寸更小。 |
  | font-stretch | 允许你使文字变宽或变窄。所有主流浏览器都不支持。 |
  | font-size-adjust | 为某个元素规定一个 aspect 值，字体的小写字母 "x" 的高度与 "font-size" 高度之间的比率被称为一个字体的 aspect 值。这样就可以保持首选字体的 x-height |

- 文本系列属性

  | 属性 | 说明 |
  | :-----: | :---- |
  | text-indent | 文本缩进 | 
  | text-align | 文本水平对齐 | 
  | line-height | 行高 | 
  | word-spacing | 增加或减少单词间的空白（即字间隔） | 
  | letter-spacing | 增加或减少字符间的空白（字符间距） | 
  | text-transform | 控制文本大小写 | 
  | direction | 规定文本的书写方向 | 
  | color | 文本颜色 | 
	 
- 元素可见性

  | 属性 | 说明 |
  | :-----: | :---- |
  |  visibility | 规定元素是否可见 | 

- 表格布局属性

  | 属性 | 说明 |
  | :-----: | :---- |
  |  caption-side | 规定表格标题的放置方式 | 
  |  border-collapse | 为表格设置合并边框模型 | 
  |  border-spacing | 设置相邻单元格的边框间的距离（仅用于“边框分离”模式） | 
  |  empty-cells | 设置是否显示表格中的空单元格（仅用于“分离边框”模式） | 
  |  table-layout | 显示表格单元格、行、列的算法规则 | 

- 列表属性

  | 属性 | 说明 |
  | :-----: | :---- |
  |  list-style-type | 设置列表项标记的类型 | 
  |  list-style-image | 使用图像来替换列表项的标记 | 
  |  list-style-position | 设置在何处放置列表项标记 | 
  |  list-style | 在一个声明中设置所有的列表属性 | 
	 
- 生成内容属性

  | 属性 | 说明 |
  | :-----: | :---- |
  |  quotes | 设置嵌套引用（embedded quotation）的引号类型 | 

- 光标属性

  | 属性 | 说明 |
  | :-----: | :---- |
  |  cursor | 规定要显示的光标的类型（形状） | 

- 页面样式属性

  | 属性 | 说明 |
  | :-----: | :---- |
  |  page | 检索或指定显示对象容器时使用的页面类型 | 
  |  page-break-inside	 | 设置元素内部的 page-breaking 行为 | 
  |  orphans | 设置或返回一个元素必须在页面底部的可见行的最小数量（用于打印或打印预览） | 

- 声音样式属性

  | 属性 | 说明 |
  | :-----: | :---- |
  |  speak |  规定内容是否将以声音形式呈现 | 
  |  speak-punctuation | 规定如何念出标点符号 | 
  |  speak-numeral | 规定如何念出数字 | 
  |  speak-header | 指定如何处理表格标题。应该在每个单元格之前朗读标题，还是仅在标题与前一个单元格不同的单元格之前念出标题。 | 
  |  speech-rate | 规定说话的速度 | 
  |  volume | 规定说话的音量 | 
  |  voice-family | 规定语音的语音家族 | 
  |  pitch | 规定说话的声音 | 
  |  pitch-range | 规定语音的变化（单调还是动听的声音？） | 
  |  stress | 规定语音中的“压力” | 
  |  richness | 指定语音的丰富程度。（声音丰富还是稀薄？） | 
  |  azimuth | 设置声音的来源 | 
  |  elevation | 设置声音的来源 | 

### 写一个方法，将下划线命名改为大驼峰命名

```js
/**
 * 写一个方法，将下划线命名改为大驼峰命名
 */

const underlineToHump = (str) =>{
    return str.split('_').map(item=>item.charAt(0).toUpperCase() + item.slice(1)).join('')
}

console.log(underlineToHump('good_good_study_day_day_up')) // GoodGoodStudyDayDayUp

/**
 * 拓展：写一个方法，将大驼峰命名改为下划线命名
 */

const humpToUnderline= (str) => {
    return str.split('').map((item,index)=> (/[A-Z]/).test(item) ? `${index?'_':''}${item.toLowerCase()}` : item).join('')
}

console.log(humpToUnderline('GoodGoodStudyDayDayUp')) // good_good_study_day_day_up
```
