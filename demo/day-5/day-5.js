/**
 * 写一个把字符串大小写切换的方法
 */

const caseSwitch=(str)=>{
    return str.split('').map(char=>{
        if((/[a-z]/).test(char)){
            return char.toUpperCase()
        }
        if((/[A-Z]/).test(char)){
            return char.toLowerCase()
        }
        return char
    }).join('')
}

console.log('原字符串 =====》','adaASdaaAdadaagaga')
console.log('转换后 =====》',caseSwitch('adaASdaaAdadaagaga'))
