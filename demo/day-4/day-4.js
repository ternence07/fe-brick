/**
 * 写一个方法，将下划线命名改为大驼峰命名
 */

const underlineToHump = (str) =>{
    return str.split('_').map(item=>item.charAt(0).toUpperCase() + item.slice(1)).join('')
}

console.log(underlineToHump('good_good_study_day_day_up')) // GoodGoodStudyDayDayUp

/**
 * 拓展：写一个方法，将大驼峰命名改为下划线命名
 */

const humpToUnderline= (str) => {
    return str.split('').map((item,index)=> (/[A-Z]/).test(item) ? `${index?'_':''}${item.toLowerCase()}` : item).join('')
}

console.log(humpToUnderline('GoodGoodStudyDayDayUp')) // good_good_study_day_day_up
