/**
 * 用递归算法实现，数组长度为5且元素的随机数在2-32间不重复的值
 */

const createArr =(min,max,len)=>{

    const randomNumber =(min,max)=>{
        return Math.floor(Math.random()*(max - 1) + 2)
    }

    const arr = new Array(len)
    const num = randomNumber(min,max)
    let i = 0

    const randomArr =(arr,num)=>{
        if (arr.indexOf(num)< 0){
            arr[i] = num;
            i++;
        } else {
            num = randomNumber(min,max);
        }
        if (i>=arr.length){
            return arr;
        }else{
            return randomArr(arr,num)
        }
    }
    return  randomArr(arr,num)
}

console.log(createArr(2,32,5))
