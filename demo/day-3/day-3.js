/**
 * 去除字符串中最后一个字符
 * 拓展：删除字符串中的某个位置的某个字符
 */

const  delLastChar = (str,char) =>{
    return str.split('').reverse().join('').replace(char,'').split('').reverse().join('')
}

console.log((delLastChar('abacadaeafagah','a')))

const group=(array, subGroupLength)=> {
    return [array.slice(0,subGroupLength),array.slice(subGroupLength)];
}

const delChar= (str,char,index) => {
    const result=[]
    const list = group(str.split(char),index)
    for (const listElement of list) {
        result.push(listElement.join(char))
    }
    return result.join('')
}

console.log((delChar('abacadaeafagah','a',10)))


